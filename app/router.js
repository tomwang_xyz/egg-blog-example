'use strict'

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app

  router.get(['/','/index.html','/index'], controller.index.index)
  router.get('/serve', controller.serve.index)
  
  router.get('/product', controller.product.index)
  router.get('/product/detail', controller.product.detail)
  router.get('/product/detail2/:id', controller.product.detail2)
  router.post('/product/create', controller.product.create)
  
  router.put('/product/update/:id', controller.product.update)
  router.del('/product/delete/:id', controller.product.delete)
  // 真实数据接口
  router.post('/article/create', controller.article.create)
  router.get('/article/lists', controller.article.lists)
  router.get('/article/detail/:id', controller.article.detail)
};
