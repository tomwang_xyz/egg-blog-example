'use strict'

const Controller = require('egg').Controller

class IndexController extends Controller {
  async index() {
    const { ctx } = this
	const res = await ctx.service.product.index()
    await ctx.render('index.html',{
		res,
		lists: ['a', 'b', 'c']
	})
  }
}

module.exports = IndexController
