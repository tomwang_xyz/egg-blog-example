'use strict'

const Controller = require('egg').Controller
const moment = require('moment')
class ArticleController extends Controller {
  async create() {
    const { ctx, app } = this
	const params = {
		...ctx.request.body,
		createTime: moment().format('YYYY-MM-DD HH:mm:ss')
	}
	const result = await ctx.service.article.create(params)
	if(result){
		ctx.body = {
			status: 200,
			data: result
		}
	}else{
		ctx.body = {
			status: 500,
			errMsg: 'create new article error'
		}
	}
  }
  async lists(){
	  const { ctx, app } = this
	  const result = await ctx.service.article.lists();
	  if(result){
	  	ctx.body = {
	  		status: 200,
	  		data: result
	  	}
	  }else{
	  	ctx.body = {
	  		status: 500,
	  		errMsg: 'lists data not defind'
	  	}
	  }
  }
  async detail(){
  	  const { ctx, app } = this
  	  const result = await ctx.service.article.detail(ctx.params.id);
  	  if(result){
  	  	ctx.body = {
  	  		status: 200,
  	  		data: result
  	  	}
  	  }else{
  	  	ctx.body = {
  	  		status: 500,
  	  		errMsg: 'this id article not defind'
  	  	}
  	  }
  }
}

module.exports = ArticleController